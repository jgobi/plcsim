const p = require('phin');

class PLCSim {
	/**
	* Driver de comnicação com o PLC Simulado
	* @param {string} [host] host ou IP do PLC simulado. Padrão: 'localhost'
	* @param {number} [port] porta do PLC simulado. Padrão: 8867
	* @param {number} [timeout] timeout das requisições em milissegundos. Padrão: 2000
	*/
	constructor(host, port, timeout) {
		this.host = host || 'localhost';
		this.port = port || 8867;
		this.timeout = timeout || 2000;
	}

	/**
	* Liga o PLC, preservando os valores das tags
	* @param {function} cb callback
	*/
	on(cb) {
		p({
			url: `http://${this.host}/on`,
			method: 'GET',
			port: this.port
		}, err => {
			if(typeof cb === 'function') return cb(err);
		})
	}

	/**
	* Desliga o PLC, preservando os valores das tags
	* @param {function} cb callback
	*/
	off(cb) {
		p({
			url: `http://${this.host}/off`,
			method: 'GET',
			port: this.port
		}, err => {
			if(typeof cb === 'function') return cb(err);
		})
	}

	/**
	* Reinicia o PLC, EXCLUINDO os valores das tags
	* e retornando-os para os valores padrão
	* @param {function} cb callback
	*/
	reset(cb) {
		p({
			url: `http://${this.host}/reset`,
			method: 'GET',
			port: this.port,
			timeout: this.timeout
		}, err => {
			if(typeof cb === 'function') return cb(err);
		})
	}

	/**
	* Le uma tag
	* @param {string} tag Tag a ser lida
	* @param {function} cb callback
	*/
	readTag(tag, cb) {
		p({
			url: `http://${this.host}/get/${tag}`,
			method: 'GET',
			port: this.port,
			timeout: this.timeout
		}, (err, res) => {
			if (typeof cb === 'function') {
				if (err) {
					return cb(err);
				} else {
					let r = JSON.parse(res.body.toString('utf8'));
					return cb(r.error !== '' ? r.error : null, r['value']);
				}
			}
		})
	}

	/**
	* Le um array de tags
	* @param {Array<string>} tags vetor das tags a serem lidas
	* @param {function} cb callback
	*/
	readTags(tags, cb) {
		p({
			url: `http://${this.host}/multiget`,
			method: 'POST',
			port: this.port,
			timeout: this.timeout,
			data: tags.join(';')
		}, (err, res) => {
			if (typeof cb === 'function') {
				if (err) {
					return cb(err);
				} else {
					let r = JSON.parse(res.body.toString('utf8'));
					return cb(r.error !== '' ? r.error : null, r.tags);
				}
			}
		})
	}

	/**
	* Escreve um valor em uma tag
	* @param {string} tag nome da tag a ser escrita
	* @param {string|number|boolean} value valor a ser escrito na tag
	* @param {function} cb callback
	*/
	writeTag(tag, value, cb) {
		p({
			url: `http://${this.host}/set/${tag}`,
			method: 'POST',
			port: this.port,
			timeout: this.timeout,
			data: String(value)
		}, (err, res) => {
			if (typeof cb === 'function') {
				if (err) {
					return cb(err);
				} else {
					let r = JSON.parse(res.body.toString('utf8'));
					return cb(r.error !== '' ? r.error : null);
				}
			}
		})
	}
}

module.exports = PLCSim
