#!/usr/bin/env node

const program = require('commander');
const path = require('path');
const fs = require('fs');

program
	.version('0.1.0', '-v, --version')
	.usage('command')

program
	.command('init <config_file>')
	.alias('config')
	.description('inicializa um arquivo de configuração para o PLC simulado')
	.action(function (config_file, options) {
		config_file = config_file.split('\\').join('_');
		if(!(/.+\.js$/.test(config_file))) config_file+='.js';

		fs.copyFile(path.resolve(__dirname, 'example.js'), path.resolve(config_file), err => {
			if(!err) {
				console.log(`Criado arquivo ${config_file} com um exemplo.`);
			} else {
				console.error(`Erro ao criar arquivo ${config_file}.`);
			}
		});
	});

program
	.command('run [config_file]')
	.description('executa simulador de PLC para um arquivo de configuração especificado')
	.option("-p, --port <port_number>")
	.action(function (config_file, options) {
		let port = options.port || 8867;
		let config = {};
		try {
			config = require(path.resolve(config_file));
		} catch (e) {
			console.warn('Arquivo de configurações não fornecido. Iniciando sem configurações.');
		}
		require('./server')(config, port)
		.then(() => {
			console.log(`Servidor de PLC iniciado na porta ${port}
Acesse http://localhost:${port}/ para ver o valor de todas as tags

Pressione Ctrl+C para sair`);
		})
		.catch(err => {
			console.log(`Falha ao iniciar servidor na porta ${port}: ${err}`);
		})
	});

program
	.command('*')
	.action(_ => program.help())

program.parse(process.argv);

if (!program.args.length) program.help();
