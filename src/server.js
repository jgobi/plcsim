const polka = require('polka');
var rawBodyParser = require('raw-body-parser');

// Se não seja passado um arquivo de configurações, use esse objeto abaixo.
let config = {
	tags: {},
	writeMiddleware: {},
	passive: []
}

let tagsInitialState = {}; // Usado para resetar o PLC, caso necessário
let serverTurnedOn = false; // Indica se o PLC deve ou não responder às solicitações

let tagFuncs = {
	// Lê uma tag
	read (tag) {
		return config.tags[tag];
	},
	// Escreve numa tag
	write (tag, value) {
		let type = typeof config.tags[tag];
		if (type !== 'undefined') {
			config.tags[tag] = cast(value, type);
			return true;
		}
		return false;
	},
	// Escreve numa tag e propaga
	writeP (tag, value) {
		if (typeof config.writeMiddleware[tag] === 'function') {
			return tagFuncs.write(tag, config.writeMiddleware[tag](value, config.tags[tag], tagFuncs))
		} else {
			return tagFuncs.write(tag, value)
		}
	}
}

let passiveFuncs = [];
let passiveObj;

// Inicia as funções passivas e retoma as respostas às solicitações
function startPLC() {
	// Inicia as funções passivas
	for (let i in config.passive) {
		if (typeof config.passive[i].exec === 'function') {
			passiveFuncs.push(
				setInterval(
					config.passive[i].exec,
					parseInt(config.passive[i].timeout) || 1000,
					tagFuncs,
					passiveObj
				)
			);
		}
	}
	serverTurnedOn = true;
}

// Para as funções passivas e para de responder solicitações
function stopPLC() {
	// Para as funções passivas
	for (let i in passiveFuncs) {
		clearInterval(passiveFuncs[i]);
	}
	passiveFuncs = [];
	serverTurnedOn = false;
}

// Reseta PLC para os valores padrão das tags e reiniciar as funções passivas
function resetPLC() {
	stopPLC();
	config.tags = JSON.parse(JSON.stringify(tagsInitialState)); // Reseta status das tags
	startPLC();
}

// Converte dados
function cast(value, type) {
	switch(type) {
		case 'string':
			return String(value);
		case 'number':
			return Number(value) || 0; // se Number retornar NaN ou 0, subsitua por 0 :)
		case 'boolean':
			if(['false','0'].indexOf(value) != -1) return false;
			else return Boolean(value);
		default:
			throw new Error("Valor não esperado para uma tag");
	}
}

// Respostas em JSON para a alegria do driver e com a codificação certa para a alegria do driver
function jsonMiddleware(req, res, next) {
	res.writeHead(200, { 'Content-Type': 'application/json; charset=utf-8' });
	next();
}

// Resposta genérica para evitar erros inesperados
function methodNotAllowedAnswer(req, res) {
	res.end(JSON.stringify({
		error: 'method not allowed'
	}))
}

function runServer (newConfig, port) {
	return new Promise(function (resolve, reject) {
		// Verifica se as tags estão bem definidas
		if (newConfig.tags) {
			for (let i in newConfig.tags) {
				if (['string','number','boolean'].indexOf(typeof newConfig.tags[i]) == -1) {
					return reject("Tags devem ter um valor inicial, e este valor deve ser uma string, um número ou um booleano.");
				}
			}
		}
		// Cria cópia do estado atual das TAGS
		if (newConfig.tags) {
			tagsInitialState = JSON.parse(JSON.stringify(newConfig.tags)); // copia objeto
			config.tags = JSON.parse(JSON.stringify(newConfig.tags)); // copia objeto
		}
		if (newConfig.writeMiddleware) config.writeMiddleware = newConfig.writeMiddleware;
		if (newConfig.passive) config.passive = newConfig.passive;

		// Inicia servidor REST
		polka()
		.use(rawBodyParser(), jsonMiddleware)
		.get('/', (req, res) => {
			res.end(JSON.stringify(config.tags, null, 2)); // monitoramento das tags (para uso externo ao driver)
		})
		.get('/get/:tag', (req, res) => { // endpoint para obter uma tag
			if (serverTurnedOn) {
				let { tag } = req.params;
				res.end(JSON.stringify({
					error: typeof config.tags[tag] === 'undefined' ? 'not found' : '',
					value: typeof config.tags[tag] !== 'undefined' ? config.tags[tag] : ''
				}));
			}
		})
		.post('/get/:tag', methodNotAllowedAnswer)
		.get('/multiget', methodNotAllowedAnswer) // endpoint para obter várias tags
		.post('/multiget', (req, res) => {
			if (serverTurnedOn) {
				let value = req.rawBody.toString('utf-8');
				let answer = {};
				let notFound = [];
				value.split(';').forEach(t => {
					answer[t] = tagFuncs.read(t)
					if(typeof answer[t] === 'undefined') notFound.push(t);
				})
				res.end(JSON.stringify({
					error: (notFound.length > 0) ? `some not found: ${notFound.join(';')}` : '',
					tags: answer
				}));
			}
		})
		.get('/set/:tag', methodNotAllowedAnswer)
		.post('/set/:tag', (req, res) => { // endpoint para escrever numa tag
			if (serverTurnedOn) {
				let { tag } = req.params;
				let value = req.rawBody.toString('utf-8');
				res.end(JSON.stringify({
					error: tagFuncs.writeP(tag, value) ? '' : 'not found'
				}))
			}
		})
		.get('/on', (req, res) => { // endpoint para ligar PLC simulado
			startPLC();
			res.end(JSON.stringify({
				msg: 'done',
				error: ''
			}));
		})
		.get('/off', (req, res) => { // endpoint para desligar PLC simulado
			stopPLC();
			res.end(JSON.stringify({
				msg: 'done',
				error: ''
			}));
		})
		.get('/reset', (req, res) => { // endpoint para dar reset no PLC simulado
			resetPLC();
			res.end(JSON.stringify({
				msg: 'done',
				error: ''
			}));
		})
		.listen(port).then(_ => { // inicia servidor
			startPLC();
			resolve();
		});

	});
}

module.exports = runServer;
