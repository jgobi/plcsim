module.exports = {
    /**
     * As tags POSSUEM tipos.
     * Eles são extraídos a partir dos valores iniciais abaixo especificados
     */
    tags: {
        TEST1: 'TEXTO',
        TEST2: 0,
        grande: false
    },
    writeMiddleware: {
        /**
         * Função de tratamento de escrita na tag <nomeDaFunção>
         * Esta função é chamada quando uma escrita nesta tag é solitada,
         * seja pelo servidor REST ou pela chamada da função writeP()
         * @param newVal Valor novo a ser inserido na tag
         * @param oldVal Valor anterior, pode ser 
         * @param funcs Funções de manipulação de tags: read, write, writeNOP
         *  - A função read retorna o valor de uma tag `let a = read('TEST1)`
         *  - A função write escreve um valor numa tag `write('TEST', "Olá")`
         *  - A função writeP funciona como a write, porém faz a escrita
         * usando a função de escrita da tag de destino, podendo ser usada
         * para fazer reações em cadeia. (Write and Propagate)
         * @returns O retorno será o valor associado à tag correspondente
         */
        TEST1 (newVal, oldVal, funcs) {
            let { read, writeP } = funcs;
            if(newVal > oldVal || read('TEST2') === 0) {
                writeP('grande', true);
                return newVal;
            } else {
                writeP('grande', false);
                return oldVal;
            }
        }
        /**
         * Se não houver função de tratamento de escrita, a escrita será
         * realizada normalmente, sem tratamento (como a escrita na tag TEST2)
         */
    },
    /**
     * Funções a serem executadas continuamente
     */
    passive: [{
        description: 'Controle de lifebit', // apenas para identificação
        interval: 1000, // intervalo em que a função deve ser executada

        /**
         * A função passiva propriamente dita
         * @param funcs Objeto com as funções de leitura/escrita nas tags do PLC
         * @param opt Um objeto compartilhado entre todas as funções passivas
         */
        exec (funcs, opt) {
            let { read, write } = funcs;
            write('TEST2', !read('TEST2'));
        }
    }]
}
