# Simulador de PLC e driver

A lightweight and lite-dependencies REST PLC Simulator.

## Features

* Super leve! Possui pouquíssimas dependências!
* Portátil - basta executar o script em node
* Genérico - É possível criar o PLC que for necessário
* Baseado em tags - fácil de iniciar e de dar manutenção
* TAGS tipadas - menos erros em sua vida
* Múltiplos simuladores em um mesmo computador - basta usar várias portas e arquivos de configurações


## Como usar

Em construção (leia os arquivos de código fonte por hora)
